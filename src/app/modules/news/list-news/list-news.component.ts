import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreateNewsComponent } from '../create-news/create-news.component';

@Component({
  selector: 'app-list-news',
  templateUrl: './list-news.component.html',
  styleUrls: ['./list-news.component.scss']
})
export class ListNewsComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {
  }

  openDialog() {
    const dialogRef = this.dialog.open(CreateNewsComponent, {
      data: {
        title: 'Hello World'
      }
    });

    dialogRef.afterClosed().subscribe(res => {
      console.log(res);
    });

  }


}
