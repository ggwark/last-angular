import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NewsRoutingModule } from './news-routing.module';
import { ListNewsComponent } from './list-news/list-news.component';
import { CreateNewsComponent } from './create-news/create-news.component';
import { MaterialModule } from 'src/app/shared/material/material.module';


@NgModule({
  declarations: [ListNewsComponent, CreateNewsComponent],
  imports: [
    CommonModule,
    NewsRoutingModule,
    MaterialModule
  ]
})
export class NewsModule { }
